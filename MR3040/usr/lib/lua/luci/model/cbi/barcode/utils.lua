function update_cron(upload_cron)
    local cronfile = "/etc/crontabs/root"
    local upload_script = '/barcode/barcode.sh upload'

    local lines = {}

    local f = io.open(cronfile, "r")
    if f then
        for line in f:lines() do
            if not line:find(upload_script) then
                lines[#lines + 1] = line
            end
        end
        f:close()
    end

    lines[#lines + 1] = string.format('*/%d\t*\t*\t*\t*\t%s', upload_cron, upload_script)

    local f = io.open(cronfile, "w")
    for _, line in pairs(lines) do
        f:write(line, '\n')
    end
    f:close()

    os.execute("/usr/bin/crontab %q" % cronfile)
end
