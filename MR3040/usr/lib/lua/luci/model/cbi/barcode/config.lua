m = Map("barcode", "General", "Please fill out the form below")

d = m:section(TypedSection, "common_conf", "Common config"); d.anonymous = true
--a = d:option(Value, "version", "Version"); a.optional=false; a.rmempty = false
a = d:option(Value, "station_id", "Station ID"); a.optional = false; a.rmempty = false
a = d:option(Value, "data_dir", "Data dir"); a.optional = false; a.rmempty = false
a = d:option(Value, "data_file_fmt", "Data file fmt"); a.optional = false; a.rmempty = false

d = m:section(TypedSection, "timeout_conf", "Timeout config"); d.anonymous = true
a = d:option(Value, "next_data_file", "Next data file (seconds)"); a.optional = false; a.rmempty = false
a = d:option(Value, "upload_cron", "Upload cron (minutes)"); a.optional = false; a.rmempty = false
a = d:option(Value, "remove_after_upload", "Remove files after upload (>minutes)"); a.optional = false; a.rmempty = false

d = m:section(TypedSection, "upload_conf", "Upload config"); d.anonymous = true
a = d:option(Value, "upload_host", "Upload host"); a.optional = false; a.rmempty = false
a = d:option(Value, "upload_dir", "Upload directory"); a.optional = false; a.rmempty = false
a = d:option(Value, "upload_key_path", "Upload key path"); a.optional = false; a.rmempty = false


m.on_after_commit = function(self)
    require("luci.model.cbi.barcode.utils")
    local upload_cron = self:get('timeout', "upload_cron")
    update_cron(upload_cron)
end

return m

