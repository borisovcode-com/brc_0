module("luci.controller.barcode.barcode", package.seeall) --notice that new_tab is the name of the file new_tab.lua
function index()
    entry({ "admin", "Barcode" }, firstchild(), "Barcode", 60).dependent = false --this adds the top level tab and defaults to the first sub-tab (tab_from_cbi), also it is set to position 30
    entry({ "admin", "Barcode", "config" }, cbi("barcode/config"), "Config", 1) --this adds the first sub-tab that is located in <luci-path>/luci-myapplication/model/cbi/myapp-mymodule and the file is called cbi_tab.lua, also set to first position
    entry({ "admin", "Barcode", "status" }, template("barcode/status"), "Status", 2) --this adds the second sub-tab that is located in <luci-path>/luci-myapplication/view/myapp-mymodule and the file is called view_tab.htm, also set to the second position
end