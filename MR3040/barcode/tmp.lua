#!/usr/bin/lua

require("luci.model.uci")
require("luci.model.cbi.barcode.utils")

local cur = uci.cursor()
local upload_cron = cur:get("barcode", "timeout", "upload_cron")
upload_cron = upload_cron + 1
print(upload_cron)
cur:set("barcode", "timeout", "upload_cron", upload_cron)
cur:save("barcode")
cur:apply()

update_cron(upload_cron)

os.execute("/sbin/luci-reload %s >/dev/null 2>&1")
