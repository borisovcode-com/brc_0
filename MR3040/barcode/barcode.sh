#!/bin/sh

HOSTNAME=$(/bin/uname -n)
CONFIG_FILE=/etc/config/barcode
NOW=$(date +%Y%m%d_%H%M%S.%s)
PRIVATE_KEY=$(cat ${CONFIG_FILE} | grep upload_key_path | tr \' ' ' | awk '{print $NF}')
PUBLIC_KEY=${PRIVATE_KEY}.pub

abspath=$0
script=$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")
action=${1:-help}
shift 2

acquire_overseer() {
    SCRIPT_PATH=/barcode/acquire.lua
    PID_PATH=/var/run/barcode_acquire.pid
    DEV_PATH=/dev/input/event0

    echo ${SCRIPT_PATH} ${PID_PATH} ${DEV_PATH}
    echo sleeping 10
    sleep 10
    echo ${SCRIPT_PATH} ${PID_PATH} ${DEV_PATH}

    # TODO remove for production
    exit 0

    while true
    do
        if [ -e ${DEV_PATH} ]
        then
            if [ ! -f ${PID_PATH} ]
            then
                lua ${SCRIPT_PATH} ${DEV_PATH} & echo $! > ${PID_PATH}
            fi
        else
            if [ -f ${PID_PATH} ]
            then
                kill -9 $(cat ${PID_PATH})
                rm ${PID_PATH}
            fi
        fi
        sleep 3
    done
}

start_acquire_overseer() {
    ${abspath} acquire_overseer &
}

_get_upload_host() {
    echo $(cat ${CONFIG_FILE} | grep upload_host | tr \' ' ' | awk '{print $NF}')
}

upload() {
    DATA_DIR=$(cat ${CONFIG_FILE} | grep data_dir | tr \' ' ' | awk '{print $NF}')
    UPLOAD_HOST=$( _get_upload_host )
    UPLOAD_DIR=$(cat ${CONFIG_FILE} | grep upload_dir | tr \' ' ' | awk '{print $NF}')

    echo ${NOW} ${UPLOAD_HOST} ${UPLOAD_DIR} ${UPLOAD_KEY_PATH} >${DATA_DIR}/${HOSTNAME}-beat.txt

    UPLOAD_AND_SAVE_FILES=$(/bin/ls -1 -t ${DATA_DIR} | head -2)
    UPLOAD_AND_REMOVE_FILES=$(/bin/ls -1 -t ${DATA_DIR} | tail +3)

    #dropbearkey -y -t rsa -f /etc/dropbear/dropbear_rsa_host_key > /root/id_rsa.pub


    echo Upload and remove
    for file in ${UPLOAD_AND_REMOVE_FILES}; do
        echo scp -i ${UPLOAD_KEY_PATH} ${DATA_DIR}/${file##*/} ${UPLOAD_HOST}:${UPLOAD_DIR}/ -- rm ${DATA_DIR}/${file##*/}
        scp -i ${UPLOAD_KEY_PATH} ${DATA_DIR}/${file##*/} ${UPLOAD_HOST}:${UPLOAD_DIR}/ && rm ${DATA_DIR}/${file##*/}
    done

    echo Upload and save
    for file in ${UPLOAD_AND_SAVE_FILES}; do
        echo scp -i ${UPLOAD_KEY_PATH} ${DATA_DIR}/${file##*/} ${UPLOAD_HOST}:${UPLOAD_DIR}/ -- rm ${DATA_DIR}/${file##*/}
        scp -i ${UPLOAD_KEY_PATH} ${DATA_DIR}/${file##*/} ${UPLOAD_HOST}:${UPLOAD_DIR}/ && rm ${DATA_DIR}/${file##*/}
    done
}

create_upload_key() {
    PRIVATE_KEY_DIR=$(dirname ${PRIVATE_KEY})
    [ ! -d ${PRIVATE_KEY_DIR} ] && mkdir ${PRIVATE_KEY_DIR}
    dropbearkey -t rsa -f ${PRIVATE_KEY}
    dropbearkey -y -t rsa -f ${PRIVATE_KEY} > ${PUBLIC_KEY}
}

upload_upload_key()
{
    UPLOAD_HOST=$( _get_upload_host )
    ssh ${UPLOAD_HOST} sh -c "'cat >> .ssh/authorized_keys'" < ${PUBLIC_KEY}
}

help() {
	cat <<EOF
Syntax: ${script} [command]

Available commands:
    upload                    Upload data files
    start_acquire_overseer    Start acquire overseer
    create_upload_key         Create upload key
    upload_upload_key         Upload upload key
EOF
}


${action} "$@"