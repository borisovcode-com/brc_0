#!  /usr/bin/lua
require("luci.model.uci")

print('Content-type: text/html\n\n')

local station_id = uci.get("barcode", "station", "station_id")
local next_data_file_timeout = uci.get("barcode", "station", "next_data_file_timeout")

print('<style>* { font-family: monospace; }</style> <body>')
print(string.format('SP/BC Barcode %s<br/>', '0.9'))
print(string.format('Station ID %s<br/>', station_id))
print(string.format('Next data file timeout %s<br/>', next_data_file_timeout))
