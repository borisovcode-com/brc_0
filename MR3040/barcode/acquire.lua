require("luci.model.uci")

function get_hostname()
    local f = io.popen("/bin/uname -n")
    local hostname = f:read("*a") or ""
    f:close()
    hostname = string.gsub(hostname, "\n$", "")
    return hostname
end

function get_version()
    return uci.get("barcode", "common", "version")
end

function get_station_id()
    return uci.get("barcode", "common", "station_id")
end

function get_timeout_next_data_file()
    return tonumber(uci.get("barcode", "timeout", "next_data_file"))
end

function get_data_file_fmt()
    return string.format('%s/%s', uci.get("barcode", "common", "data_dir"), uci.get("barcode", "common", "data_file_fmt"))
end

function log(fmt, ...)
    print(string.format(fmt, unpack(arg)))
end

function save_station_id(station_id)
    local cur = uci.cursor()
    cur:set("barcode", "common", "station_id", station_id)
    cur:save("barcode")
    cur:apply()
    os.execute("/sbin/luci-reload %s >/dev/null 2>&1")
end

function save_string_to_data_file(ts, s)
    local f = io.open(string.format(config.get_data_file_fmt(), get_hostname(), ts), "a")
    f:write(s)
    f:close()
end


local dev = arg[1]
log('Barcode Acquire (hostname: %s, station id %s, device %s)', get_hostname(), get_station_id(), dev)

local f = assert(io.open(dev, "rb"))
local block_size = 16
local barcode = ""
local ts = os.time()
while true do
    local raw = f:read(block_size)
    if not raw then break end
    local s = ""
    for b in string.gfind(raw, ".") do
        s = s .. string.format("%01X", string.byte(b))
    end
    local sec = tonumber(s:sub(1, 8), 8)
    local msec = tonumber(s:sub(9, 16), 8)
    local type = tonumber(s:sub(17, 20), 8)
    local code = tonumber(s:sub(21, 24), 8)
    local value2 = tonumber(s:sub(29, 32), 8)
    if type == 1 and value2 == 1 then
        if code == 0x1C then
            if barcode:sub(1, 3) == "000" then
                station_id = barcode:sub(barcode:len() - 1, barcode:len())
                save_station_id(station_id)
                io.write(string.format('Set station id %s', station_id), '\n')
            else
                local id = barcode:sub(1, barcode:len() - 1)
                local out_string = string.format('%s %d.%d %s %s\n', station_id, sec, msec, id, get_hostname())
                io.write(out_string)
                save_string_to_data_file(ts, out_string)
                if os.time() - ts > 61 then
                    ts = os.time()
                end
            end
            barcode = ''
        else
            if code == 0xB then
                barcode = string.format("%s0", barcode)
            elseif code then
                barcode = string.format("%s%d", barcode, code - 1)
            end
        end
    end
end
