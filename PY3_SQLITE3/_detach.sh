#!/bin/sh
ORESYS_BARCODE_LOG="/var/log/oresys_barcode.log"
echo `date +"%F%T"` DETACH BARCODE >>${ORESYS_BARCODE_LOG}
cd /home/barcode/oresys_barcode
pkill -f "acquire.py /dev/ukbd0"
/etc/rc.d/syscons setkeyboard /dev/kbd0

