#!/bin/sh
ORESYS_BARCODE_LOG="/var/log/oresys_barcode.log"
echo `date +"%F%T"` ATTACH BARCODE >> ${ORESYS_BARCODE_LOG}
sleep 5
cd /home/barode/oresys_barcode
kbdcontrol -A ukbd0 </dev/ttyv0
/usr/local/bin/python3.5 acquire.py /dev/ukbd0

