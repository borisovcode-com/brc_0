import logging

formatter = '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'
logging.basicConfig(level=logging.DEBUG,
                    format=formatter,
                    # datefmt='%m-%d %H:%M',
                    filename='/var/log/oresys_barcode.log',
                    filemode='a')
# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.ERROR)
formatter = logging.Formatter(format)
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)


def debug(message):
    logging.debug(message)


def info(message):
    logging.info(message)


def warn(message):
    logging.warn(message)


def error(message):
    logging.error(message)


def critical(message):
    logging.critical(message)
