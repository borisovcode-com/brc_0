import sqlite3
import os
import argparse
import uuid
from datetime import datetime

DBFILENAME = 'oresys_barcode.sqlite3'
STATE_UNKNOWN = 0
STATE_APPEND = 1
STATE_UPLOADED = 2


def get_db_filename():
    return os.path.join('/home/barcode/oresys_barcode', DBFILENAME)

def get_db():
    db_filename = get_db_filename()
    db = sqlite3.connect(db_filename)
    return db


def get_station_id(db):
    c = db.cursor()
    c.execute('SELECT val FROM settings WHERE code = ?', ['STATION_ID'])
    station_id = c.fetchone()[0]
    return station_id


def append(db, barcode):
    c = db.cursor()
    c.execute('SELECT MAX(id) FROM barcode')
    id = c.fetchone()[0]
    c.execute('INSERT INTO barcode VALUES (?, ?, ?, ?, ?, ?)', [id + 1, str(uuid.uuid1()), get_station_id(db), datetime.now(), barcode, 1])
    db.commit()
    c.close()


def get_list_by_state(db, state=None, limit=16):
    c = db.cursor()
    if state is None:
        c.execute('SELECT id, uid, station_id, dt, barcode, state FROM barcode ORDER BY dt DESC LIMIT ?', [limit])
    else:
        c.execute('SELECT id, uid, station_id, dt, barcode, state FROM barcode WHERE state = ? ORDER BY dt DESC LIMIT ?', [state, limit])
    ret = c.fetchall()
    c.close()
    return ret


def set_state(db, uid, state):
    c = db.cursor()
    c.execute('UPDATE barcode SET state = ? WHERE uid = ?', [state, uid])
    db.commit()
    c.close()


def create(db):
    c = db.cursor()
    c.execute('CREATE TABLE settings (code TEXT, val TEXT)')
    c.execute('INSERT INTO settings VALUES (?,?)', ['STATION_ID', ''])
    db.commit()
    c.execute('CREATE TABLE barcode (id INTEGER, uid TEXT, station_id TEXT, dt TEXT, barcode TEXT, state INTEGER)')
    c.execute('INSERT INTO barcode VALUES (?, ?, ?, ?, ?, ?)', [0, str(uuid.uuid1()), get_station_id(db), datetime.now(), '', 1])
    db.commit()
    c.close()


def action_create(args):
    print('Create database.')
    db = get_db()
    create(db)
    print('Done')


def action_list(args):
    db = get_db()
    rows = get_list_by_state(db)
    from beautifultable import BeautifulTable
    table = BeautifulTable(160)
    table.column_headers = ['id', 'station_id', 'dt', 'barcode', 'state', 'uid']
    for row in reversed(rows):
        table.append_row(row)
    print(table)


def set_station_id(db, station_id):
    c = db.cursor()
    c.execute('UPDATE settings SET val = ? WHERE code = ?', [station_id, 'STATION_ID'])
    db.commit()
    c.close()


def action_setstationid(args):
    print('set station id to {0}'.format(args.station_id))
    db = get_db()
    set_station_id(db, args.station_id)


def action_getstationid(args):
    db = get_db()
    print(get_station_id(db))


def main():
    parser = argparse.ArgumentParser('ORESYS BARCODE database manager')
    subparser = parser.add_subparsers()

    parser_create = subparser.add_parser('create')
    parser_create.set_defaults(func=action_create)

    parser_list = subparser.add_parser('list')
    parser_list.set_defaults(func=action_list)

    parser_setid = subparser.add_parser('setstationid')
    parser_setid.add_argument('station_id')
    parser_setid.set_defaults(func=action_setstationid)

    parser_getid = subparser.add_parser('getstationid')
    parser_getid.set_defaults(func=action_getstationid)

    args = parser.parse_args()
    try:
        args.func(args)
    except AttributeError:
        args.print_help()


if __name__ == '__main__':
    main()
