#!/usr/local/bin/python3.5
import logger
import pymysql.cursors
import pymysql.cursors
from db import get_db, get_list_by_state, set_state, STATE_APPEND, STATE_UPLOADED


def main():
    connection = pymysql.connect(**{})
    sql = 'INSERT INTO oresys_barcode (id, uid, station, dt, barcode, state) VALUES (%s, %s, %s, %s, %s, %s)'
    db = get_db()
    rows = get_list_by_state(db, STATE_APPEND)
    try:
        with connection.cursor() as cursor:
            while len(rows) > 0:
                for row in rows:
                    logger.info('push row {}'.format(row))
                    try:
                        cursor.execute(sql, row)
                        set_state(db, row[1], STATE_UPLOADED)
                        connection.commit()
                    except pymysql.err.IntegrityError as e:
                        logger.error('pymysql.err.IntegrityError {0}'.format(str(e)))
                        continue

                rows = get_list_by_state(db, STATE_APPEND)
    finally:
        connection.close()


if __name__ == '__main__':
    main()
