#!/usr/local/bin/python3.5

import argparse
import sys

import logger
from db import get_db, append

out = open('out.txt', 'w')


def print_out(s):
    sys.stdout.write(s)
    sys.stdout.flush()
    out.write(s)
    out.flush()


def get_code(a):
    v = {
        0x0b8b: '0',
        0x39b9: ' ',
        0x0282: '1',
        0x0383: '2',
        0x0484: '3',
        0x0585: '4',
        0x0686: '5',
        0x0787: '6',
        0x0888: '7',
        0x0989: '8',
        0x0a8a: '9',
    }
    s = ''
    for i in a:
        if i in v.keys():
            s += v[i]
    return s


def main():
    parser = argparse.ArgumentParser('Oresys barcode acquire')
    parser.add_argument('device', help='USB keyboard device, example: /dev/ukbd0')
    args = parser.parse_args()

    pipe = open(args.device, 'rb')

    db = get_db()

    a = []
    bbb = []
    while True:
        bb = pipe.read(2)
        b = int.from_bytes(bb, byteorder='big', signed=False)
        bbb.append(b)
        a.append(b)
        if b == 0x1c9c:
            barcode = get_code(a)
            logger.info('append {} from {}'.format(barcode, ''.join(['{0:04x} '.format(b) for b in bbb])))
            append(db, barcode)
            a = []
            bbb = []


if __name__ == '__main__':
    main()
