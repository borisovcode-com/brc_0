import os
import socket
import sqlite3
from datetime import datetime

import db


# from http://commandline.org.uk/python/how-to-find-out-ip-address-in-python/
def get_network_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('192.168.2.4', 22))
    return s.getsockname()[0]


def main():
    ans = ''
    d = db.get_db()
    while ans == '':
        try:
            print("\nORESYS BARCODE STATION #{} {} {}".format(db.get_station_id(d), get_network_ip(), datetime.now()))
        except sqlite3.OperationalError:
            print("\nUNKNWON BARCODE STATION\nmay need to recreate database")
        print("""
        1. Set station ID
        2. Show list
        3. Create database
        4. Show log
        0. Exit to shell

        9. Recreate database (!)
        """)
        ans = input("Please enter number of menu ")
        if ans == "1":
            station_id = input('Please write station ID (xx): ')
            db.set_station_id(d, station_id)
        elif ans == "2":
            rows = db.get_list_by_state(d)
            from beautifultable import BeautifulTable
            table = BeautifulTable()
            table.column_headers = ['id', 'station_id', 'dt', 'barcode', 'state', 'uid']
            for row in rows:
                table.append_row(row)
            print(table)
        elif ans == "3":
            db.create(d)
        elif ans == "4":
            with open('/var/log/oresys_barcode.log', 'rt') as f:
                fcontent = f.read()
                print(fcontent)
        elif ans == "0":
            print("\n")
            return
        elif ans == "9":
            d.close()
            os.remove(db.get_db_filename())
            d = db.get_db()
            db.create(d)
            print("\n")
        else:
            print("\n Not valid choice. Please try again")
        ans = ''


if __name__ == '__main__':
    main()
