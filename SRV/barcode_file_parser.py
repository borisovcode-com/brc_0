#!/usr/local/bin/python3.6
from datetime import datetime
from pathlib import Path
from uuid import uuid4

import pymysql.cursors

BASE_DIR = Path(__file__).resolve().parent

FILES_ROOT = BASE_DIR / 'barcode_data'
UPLOADED_DIR = FILES_ROOT / 'uploaded'
PARSED_DIR = FILES_ROOT / 'parsed'
MYSQL_PARAMS = {
    'host': '',
    'user': '',
    'password': '',
    'db': ''
}


def main():
    uploaded_list = [x for x in UPLOADED_DIR.iterdir() if x.is_file()]
    uploaded_list = sorted(uploaded_list, key=lambda x: x.stat().st_mtime)

    connection = pymysql.connect(**MYSQL_PARAMS)
    sql = 'INSERT INTO oresys_barcode (id, uid, station, dt, barcode, state) VALUES (%s, %s, %s, %s, %s, %s)'

    for x in uploaded_list:
        name = x.name
        if 'beat' in name:
            continue
        _, ts, _ = name.split('.')
        dt = datetime.fromtimestamp(float(ts))
        # print('{} {} {}'.format(name, dt))
        with x.open() as f:
            with connection.cursor() as cursor:
                row_id = 0
                for line in f:
                    values = line.split(' ')
                    row_id += 1
                    row = [row_id, str(uuid4()), values[0], values[1], values[2], 4]
                    # row[3] = datetime.fromtimestamp(float(row[3]) + 90653)
                    row[3] = datetime.fromtimestamp(float(row[3]))
                    row[4] = '{}X'.format(row[4])
                    print('push row {}'.format(row))
                    # logger.info('push row {}'.format(row))
                    try:
                        cursor.execute(sql, row)
                        # set_state(db, row[1], STATE_UPLOADED)
                        connection.commit()
                    except pymysql.err.IntegrityError as e:
                        print('pymysql.err.IntegrityError {0}'.format(str(e)))
                        # logger.error('pymysql.err.IntegrityError {0}'.format(str(e)))
                        continue


if __name__ == '__main__':
    main()
