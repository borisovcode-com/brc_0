import random
import unittest

import barcode_file_parser as main


class MyTestCase(unittest.TestCase):
    def setUp(self):
        main.BASE_DIR = main.FILES_ROOT / 'tmp_barcode_data'
        main.UPLOADED_DIR = main.FILES_ROOT / 'uploaded'
        main.PARSED_DIR = main.FILES_ROOT / 'parsed'
        main.FILES_ROOT.mkdir(exist_ok=True)
        main.UPLOADED_DIR.mkdir(exist_ok=True)
        main.PARSED_DIR.mkdir(exist_ok=True)

        devices = set()
        for x in range(0,random.randrange(start=2,stop=20)):
            devices.add('MR3040-{:02d}'.format(random.randrange(start=2,stop=20)))
        devices = sorted(devices)
        print(devices)

    def test_something(self):
        pass


if __name__ == '__main__':
    unittest.main()
